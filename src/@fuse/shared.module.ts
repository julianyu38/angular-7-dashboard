import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { FuseDirectivesModule } from '@fuse/directives/directives';
import { FusePipesModule } from '@fuse/pipes/pipes.module';
import { jqxChartComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxchart';
import { jqxBarGaugeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge';
import { jqxGaugeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgauge';
import { jqxProgressBarComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxprogressbar';
import { jqxRatingComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxrating';
import { jqxDataTableComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatatable';
@NgModule({
    declarations:[
        jqxChartComponent,
        jqxBarGaugeComponent,
        jqxGaugeComponent,
        jqxProgressBarComponent,
        jqxRatingComponent,
        jqxDataTableComponent
    ],
    imports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        FuseDirectivesModule,
        FusePipesModule
    ],
    exports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        FuseDirectivesModule,
        FusePipesModule,
        jqxChartComponent,
        jqxBarGaugeComponent,
        jqxGaugeComponent,
        jqxProgressBarComponent,
        jqxRatingComponent,
        jqxDataTableComponent
    ]
})
export class FuseSharedModule
{
}
