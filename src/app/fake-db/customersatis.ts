export class CustomersatisDb {
    public static widgets = {
        widget4: {
            visits: {
                value: 882,
                ofTarget: -9
            },
            chartType: "bar",
            datasets: [
                {
                    label: "Visits",
                    data: [32, 28, 27, 63, 56, 67]
                }
            ],
            labels: [
                "JAN",
                "FEB",
                "MAR",
                "APR",
                "MAY",
                "JUN"
                
            ],
            colors: [
                {
                    borderColor: "#f44336",
                    backgroundColor: "#f44336"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            gridLines:{
                                display: false
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks: {
                                min: 0,
                                max: 100,
                                step:20
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: "line",
            datasets: {
                yesterday: [
                    {
                        label: "Visitors",
                        data: [
                            190,
                            300,
                            340,
                            220,
                            290,
                            390,
                            250,
                            380,
                            410,
                            380,
                            320,
                            290
                        ],
                        fill: "start"
                    },
                    {
                        label: "Page views",
                        data: [
                            2200,
                            2900,
                            3900,
                            2500,
                            3800,
                            3200,
                            2900,
                            1900,
                            3000,
                            3400,
                            4100,
                            3800
                        ],
                        fill: "start"
                    }
                ],
                today: [
                    {
                        label: "Visitors",
                        data: [
                            410,
                            380,
                            320,
                            290,
                            190,
                            390,
                            250,
                            380,
                            300,
                            340,
                            220,
                            290
                        ],
                        fill: "start"
                    },
                    {
                        label: "Page Views",
                        data: [
                            3000,
                            3400,
                            4100,
                            3800,
                            2200,
                            3200,
                            2900,
                            1900,
                            2900,
                            3900,
                            2500,
                            3800
                        ],
                        fill: "start"
                    }
                ]
            },
            labels: [
                "12am",
                "2am",
                "4am",
                "6am",
                "8am",
                "10am",
                "12pm",
                "2pm",
                "4pm",
                "6pm",
                "8pm",
                "10pm"
            ],
            colors: [
                {
                    borderColor: "#3949ab",
                    backgroundColor: "#3949ab",
                    pointBackgroundColor: "#3949ab",
                    pointHoverBackgroundColor: "#3949ab",
                    pointBorderColor: "#ffffff",
                    pointHoverBorderColor: "#ffffff"
                },
                {
                    borderColor: "rgba(30, 136, 229, 0.87)",
                    backgroundColor: "rgba(30, 136, 229, 0.87)",
                    pointBackgroundColor: "rgba(30, 136, 229, 0.87)",
                    pointHoverBackgroundColor: "rgba(30, 136, 229, 0.87)",
                    pointBorderColor: "#ffffff",
                    pointHoverBorderColor: "#ffffff"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: "nearest",
                    mode: "index",
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontColor: "rgba(0,0,0,0.54)"
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 1000
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        }
    };
}