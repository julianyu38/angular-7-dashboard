import { InMemoryDbService } from 'angular-in-memory-web-api';
import { DashboardDb } from './dashboard';
import { ServiceavailDb } from './serviceavail';
import { CustomersatisDb } from './customersatis';
import { SocialmediaDb } from './socialmedia';


export class FakeDbService implements InMemoryDbService{
    createDb(): any{
        return {
            //Dashboards
            'dashboard-widgets' : DashboardDb.widgets,
            'serviceavail-widgets' : ServiceavailDb.widgets,
            'customersatis-widgets' : CustomersatisDb.widgets,
            'socialmedia-widgets' : SocialmediaDb.widgets
        }
    }
}