import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { LifecycledashboardComponent } from './lifecycledashboard.component';

const routes = [
    {
        path     : 'lifecycledashboard',
        component: LifecycledashboardComponent
    }
];

@NgModule({
    declarations: [
        LifecycledashboardComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        LifecycledashboardComponent
    ]
})

export class LifecycledashboardModule
{
}
