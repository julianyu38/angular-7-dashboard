import { Component } from "@angular/core";

import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";

@Component({
    selector: "lifecycledashboard",
    templateUrl: "./lifecycledashboard.component.html",
    styleUrls: ["./lifecycledashboard.component.scss"]
})
export class LifecycledashboardComponent {
    getWidth(): any {
        if (document.body.offsetWidth < 1270) {
            return "90%";
        }

        return "100%";
    }

    columns: any[] = [
        {
            text: "#",
            dataField: "#",
            width: '10%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Server",
            dataField: "Server",
            width: '15%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Description",
            dataField: "Description",
            width: '25%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Time to Fix",
            dataField: "Time to Fix",
            width: '15%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Criticality",
            dataField: "Criticality",
            width: '15%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "LongTerm Recommendation",
            dataField: "LongTerm Recommendation",
            width: '20%',
            align: "center",
            cellsAlign: "center"
        }
    ];
    constructor() {}
}
