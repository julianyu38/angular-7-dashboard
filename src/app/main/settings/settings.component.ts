import { Component } from "@angular/core";

import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";

@Component({
    selector: "settings",
    templateUrl: "./settings.component.html",
    styleUrls: ["./settings.component.scss"]
})
export class SettingsComponent {
    constructor() {}
}
