import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { IncidentdashboardComponent } from './incidentdashboard.component';

const routes = [
    {
        path     : 'incidentdashboard',
        component: IncidentdashboardComponent
    }
];

@NgModule({
    declarations: [
        IncidentdashboardComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        IncidentdashboardComponent
    ]
})

export class IncidentdashboardModule
{
}
