import { Component } from "@angular/core";

import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";

@Component({
    selector: "incidentdashboard",
    templateUrl: "./incidentdashboard.component.html",
    styleUrls: ["./incidentdashboard.component.scss"]
})
export class IncidentdashboardComponent {
    getWidth(): any {
        if (document.body.offsetWidth < 1270) {
            return "90%";
        }

        return "100%";
    }
    widget5SelectedDay = 'today';
    columns: any[] = [
        {
            text: "#",
            dataField: "#",
            width: '10%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Device",
            dataField: "Device",
            width: '20%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Description",
            dataField: "Description",
            width: '20%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Time to Fix",
            dataField: "Time to Fix",
            width: '15%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "Criticality",
            dataField: "Criticality",
            width: '20%',
            align: "center",
            cellsAlign: "center"
        },
        {
            text: "LongTerm Recommendation",
            dataField: "LongTerm Recommendation",
            width: '15%',
            align: "center",
            cellsAlign: "center"
        }

    ];
    constructor() {}
}
