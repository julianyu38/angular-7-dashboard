import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { CustomersatisComponent } from './customersatis.component';
import { MatFormFieldModule, MatIconModule, MatSelectModule } from '@angular/material';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BarRatingModule } from "ngx-bar-rating";
import { CustomersatisService } from './customersatis.service';

const routes = [
    {
        path     : 'customersatis',
        component: CustomersatisComponent,
        resolve : {
            data: CustomersatisService
        }
    }
];

@NgModule({
    declarations: [
        CustomersatisComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        FuseSharedModule,
        MatIconModule,
        MatFormFieldModule,
        MatSelectModule,
        ChartsModule,
        NgxChartsModule,
        BarRatingModule
    ],
    bootstrap   : [
        CustomersatisComponent      
    ],
    providers : [
        CustomersatisService
    ],
    exports     : [
        CustomersatisComponent
    ]
})

export class CustomersatisModule
{
}
