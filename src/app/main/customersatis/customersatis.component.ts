import { Component, OnInit } from "@angular/core";
import { CustomersatisService } from "./customersatis.service";
@Component({
    selector: "customersatis",
    templateUrl: "./customersatis.component.html",
    styleUrls: ["./customersatis.component.scss"]
})
export class CustomersatisComponent implements OnInit {
    widgets: any;
    widget1SelectedYear = "2016";
    widget5SelectedDay = "today";

    //jqxChart

    ssc_source: any = [
        { Browser: "Generation Z", Share: 4250 },
        { Browser: "Over 50s", Share: 3970 },
        { Browser: "Under 25s", Share: 3454 },
        { Browser: "26-49 ", Share: 2390 }
    ];
    ssc_dataAdapter: any = new jqx.dataAdapter(this.ssc_source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.ssc_source.url + '" : ' + error);
        }
    });
    ssc_padding: any = { left: 5, top: 25, right: 5, bottom: 5 };
    ssc_titlePadding: any = { left: 0, top: 20, right: 0, bottom: 10 };
    
    ssc_seriesGroup: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 80,
                    initialAngle: 300,
                    radius: 120,
                    innerRadius: 60,
                    centerOffset: 0,
                    offsetY: 120,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    formatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.ssc_source.length; index++) {
                            sum += this.ssc_source[index].Share;
                        }
                        return (this.ssc_source[itemIndex].Share/sum * 100) .toFixed(1)+ "%";
                    },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.ssc_source.length; index++) {
                            sum += this.ssc_source[index].Share;
                        }
                        return value + ": "+ (this.ssc_source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    ssc_legendLayout: any = {
        left: document.body.offsetHeight/2-80,
        top: 270,
        width: 200,
        height: 240,
        flow: "vertical"
    };


    // jqxChart
    ngs_source: any = [
        { Browser: "Generation Z", Share: 4250 },
        { Browser: "Over 50s", Share: 3970 },
        { Browser: "Under 25s", Share: 3454 },
        { Browser: "26-49 ", Share: 2390 }
    ];
    ngs_dataAdapter: any = new jqx.dataAdapter(this.ngs_source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.ngs_source.url + '" : ' + error);
        }
    });
    ngs_padding: any = { left: 5, top: 25, right: 5, bottom: 5 };
    ngs_titlepadding: any = { left: 0, top: 20, right: 0, bottom: 10 };
    
    ngs_seriesGroup: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 48,
                    initialAngle: 300,
                    radius: 80,
                    innerRadius:30,
                    centerOffset: 0,
                    offsetY: 80,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    formatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.ngs_source.length; index++) {
                            sum += this.ngs_source[index].Share;
                        }

                        
                        return (this.ngs_source[itemIndex].Share/sum * 100) .toFixed(1)+ "%";
                    },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.ngs_source.length; index++) {
                            sum += this.ngs_source[index].Share;
                        }

                        
                        return value + ": "+ (this.ngs_source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    ngs_legendLayout: any = {
        left: 170,
        top: 180,
        width: 240,
        height: 300,
        flow: "vertical"
    };

    // chart2

    user_journey_source: any = [
        { Browser: "Homepage Only", Share: 4250 },
        { Browser: "Home & Shop", Share: 3970 },
        { Browser: "Shop", Share: 3454 },
        { Browser: "Basket & Exit", Share: 2390 }
    ];
    user_journey_dataAdapter: any = new jqx.dataAdapter(this.user_journey_source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.user_journey_source.url + '" : ' + error);
        }
    });
    user_journey_padding: any = { left: 5, top: 25, right: 5, bottom: 5 };
    user_journey_titlepadding: any = { left: 0, top: 20, right: 0, bottom: 10 };
    
    user_journey_seriesGroup: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 48,
                    initialAngle: 300,
                    radius: 80,
                    innerRadius: 30,
                    centerOffset: 0,
                    offsetY: 80,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    formatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.user_journey_source.length; index++) {
                            sum += this.user_journey_source[index].Share;
                        }
                        return (this.user_journey_source[itemIndex].Share/sum * 100) .toFixed(1)+ "%";
                    },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.user_journey_source.length; index++) {
                            sum += this.user_journey_source[index].Share;
                        }
                        return value + ": "+ (this.user_journey_source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    user_journey_legend_layout: any = {
        left: 170,
        top: 180,
        width: 200,
        height: 300,
        flow: "vertical"
    };
    //ProgressBar

    horizontalColorRanges1 = [{ stop: 100, color: "#aa66cc" }];
    horizontalColorRanges2 = [{ stop: 100, color: "#ff4444" }];
    horizontalColorRanges3 = [{ stop: 100, color: "#f0ad4e" }];

    // right Chart

    rightchartlabels = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July"
    ];
    rightchartseries = ["Loss of customer satisfication"];
    rightchartdata = [[65, 59, 80, 81, 56, 55, 40]];
    rightchartoptions = {
        scales: {
            yAxes: [
                {
                    id: "y-axis-1",
                    type: "linear",
                    display: true,
                    position: "left"
                },
                {
                    id: "y-axis-2",
                    type: "linear",
                    display: true,
                    position: "right"
                }
            ]
        }
    };

    reviewHtml = "aaa";

    makeReview() {
        var Company = 'salmansherwani97';
        var Prod_id = '';
        var Asin = 'B0799979K9';
        var Parent_id = '';
        var Marketplace = '';   //Amazon UK
        var Language = '';      //EN
        var Date_Format = '';
        var Modal = '';
        var Modal_Link_Text='See reviews';
        var Modal_Link_Type='3';
        var Height='300px';
        var Review_Per_Page = 10;
        var Star_Color = '';

        // let Company = 'testco';
        // let Modal = ''; //Default is no if left blank 
        // let Modal_Link_Text = 'See reviews'; //Default is blank. Leave blank to hide popup link.
        // let Modal_Link_Type = '3'; //1 for anchor(Default),2 for span ,3 for button
        // let Height = '375px'; // provide height with unit like px,em,%  works only for non model version
        // let Marketplace = '';
        // let Language = '';
        // let Prod_id = '';
        // let Asin = '';
        // let Parent_id = 'NN.XX.BA.SSC';
        // let Date_Format = 'Y-m-d' //;
        // let Review_Per_Page = 20;
        // let Star_Color = ''; // use color name or color code


        // Do Not modify anything below 

        let cnt = new Date().getUTCMilliseconds();
        this.generateReview(window, document, 'script', 'amz-js', Height, Modal, Modal_Link_Text, Modal_Link_Type, Company, Marketplace, Language, Prod_id, Asin, Parent_id, Date_Format, Review_Per_Page, Star_Color, cnt);
    }

    generateReview(w, d, s, id, ht, md, mdt, mdlt, c, m, l, p, a, pi, df, pg, cl, cnt) {
        var ul = window.location.href;
        var arr = ul.split("/");
        id = id + cnt;
        var me = document.currentScript;
        var amzjs = d.createElement("div");
        amzjs.id = "'arf_container_" + cnt;
        
        me.parentNode.insertBefore(amzjs, me);
        id = id + cnt;
        if (arr[0] == 'http:' || arr[0] == 'https:') { }
        else {
            arr[0] = 'http:';
        }
        ul = arr[0] + "//fetch.areviewfetch.com/snipserv/";
        var js;
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.onload = () => {
            var tpl = d.createElement("div");
            tpl.className = 'arf-container';
            tpl.innerHTML = window["amz_tpl_data_" + cnt];

            document.getElementById("aaa").innerHTML = tpl.innerHTML;
            
            this.reviewHtml = tpl;
            

            window.setTimeout(() => {
                (document.getElementsByClassName("arf-summary")[0] as HTMLDivElement).style.display = "none";
            }, 100);
            return;
            amzjs.parentNode.insertBefore(tpl, amzjs);

            md = d.getElementById(cnt);
            if (md) { d.body.appendChild(md) };
            window["init_" + cnt]();
        };
        ul += '?company=' + c;
        ul += '&height=' + ht;
        ul += '&height=' + encodeURIComponent(ht);
        ul += '&modal=' + md;
        ul += '&modal_text=' + mdt;
        ul += '&modal_link_type=' + mdlt;
        ul += '&marketplace=' + m;
        ul += '&language=' + l;
        ul += '&Parent_id=' + pi;
        ul += '&prod_id=' + p;
        ul += '&asin=' + a;
        ul += '&date_format=' + df;
        ul += '&review_per_page=' + pg;
        ul += '&color=' + encodeURIComponent(cl);
        ul += '&cnt=' + cnt;
        js.src = ul;
        amzjs.appendChild(js);

        
    }



    constructor( private _customersatisservice: CustomersatisService ) {  }

    ngOnInit() {
        this.widgets = this._customersatisservice.widgets;
        this.makeReview();
    }
}
