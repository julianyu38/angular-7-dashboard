import { Component, OnInit } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';


@Component({
    selector: 'serviceavailtwo',
    templateUrl: './serviceavailtwo.component.html',
    styleUrls: ['./serviceavailtwo.component.scss']
})
export class ServiceavailtwoComponent implements OnInit {
    constructor(
    ) {
    }

    ngOnInit() {
        var ProgressBar = require('progressbar.js');
        var container = document.getElementById('container');
        var bar = new ProgressBar.SemiCircle(container, {
            strokeWidth: 6,
            color: '#3db5e5',
            trailColor: '#eee',
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            svgStyle: null,
            text: {
                value: '',
                alignToBottom: false
            },
            from: { color: '#ED6A5A' },
            to: { color: '#3db5e5' },
            // Set default step function for all animate calls
            step: (state, bar) => {
                bar.path.setAttribute('stroke', state.color);
                var value = Math.round(bar.value() * 100);
                if (value === 0) {
                    bar.setText('');
                } else {
                    bar.setText(value + '%');
                }

                bar.text.style.color = state.color;
            }
        });
        bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
        bar.text.style.fontSize = '2rem';

        bar.animate(1.0);  // Number from 0.0 to 1.0
    }
}
