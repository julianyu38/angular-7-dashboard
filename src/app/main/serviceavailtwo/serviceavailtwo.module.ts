import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { FuseSharedModule } from "@fuse/shared.module";

import { ServiceavailtwoComponent } from "./serviceavailtwo.component";
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatSelectModule, MatTabsModule } from '@angular/material';
import { NgCircleProgressModule } from 'ng-circle-progress';
const routes = [
    {
        path: "serviceavailtwo",
        component: ServiceavailtwoComponent
    }
];

@NgModule({
    declarations: [ServiceavailtwoComponent],
    imports: [RouterModule.forChild(routes), TranslateModule, FuseSharedModule, MatFormFieldModule,
        MatIconModule,
        MatSelectModule,
        MatButtonModule,
        MatMenuModule,
        MatTabsModule,
        NgCircleProgressModule],
    exports: [ServiceavailtwoComponent]
})
export class ServiceavailtwoModule { }
