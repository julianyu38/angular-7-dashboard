import { Component, OnInit,  ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd,Event, NavigationStart,NavigationCancel, NavigationError, RouterEvent } from '@angular/router';
import { Chart } from 'chart.js';
import { DashboardService } from 'app/main/dashboard/dashboard.service';
@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
    private twitter: any;

    widgets: any;
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';

    //Horizontal1,2,3 ProgressBar
    horizontalColorRanges1 = [{ stop: 100, color: '#aa66cc' }];
    horizontalColorRanges2 = [{ stop: 100, color: '#ff4444' }];
    horizontalColorRanges3 = [{ stop: 100, color: '#f0ad4e' }];


    makeSalesChart(){
        var canvas: any = document.getElementById("salesChart");
        var context = canvas.getContext("2d");
        canvas.height = "256px";
        var actual_gradientStroke = context.createLinearGradient(0,0,0,400);
        actual_gradientStroke.addColorStop(0, "rgba(250,174,50,1)");
        actual_gradientStroke.addColorStop(1, 'rgba(250,174,50,0)');

        var actual_gradientFill = context.createLinearGradient(0,0,0,400);
        actual_gradientFill.addColorStop(0, "rgba(250,174,50,1)");
        actual_gradientFill.addColorStop(1, "rgba(250,174,50,0)");

        var forecast_gradientStroke = context.createLinearGradient(0, 0, 0, 400);
        forecast_gradientStroke.addColorStop(0, "rgba(183,115,222,1)");
        forecast_gradientStroke.addColorStop(1, "rgba(183,115,222,0)");

        var forecast_gradientFill = context.createLinearGradient(0,0,0,400);
        forecast_gradientFill.addColorStop(0, "rgba(183,115,222,1)");
        forecast_gradientFill.addColorStop(1, "rgba(183,115,222,0)");

        var salesChart = new Chart(context, {
            type:'line',
            data: {
                labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN"],
                datasets: [{
                    label: 'Sales Actual',
                    labelColor: "rgba(255,255,255,1)",
                    borderColor: actual_gradientStroke,
                    pointBorderColor: actual_gradientStroke,
                    pointBackgroundColor: actual_gradientStroke,
                    pointHoverBackgroundColor: actual_gradientStroke,
                    pointHoverBorderColor: actual_gradientStroke,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: actual_gradientFill,
                    borderWidth: 4,
                    data: [12, 23, 11, 10, 8, 7]
                },
                {
                    label: 'Sales Forecast',
                    labelColor: "rgba(255,255,255,1)",
                    borderColor: forecast_gradientStroke,
                    pointBorderColor: forecast_gradientStroke,
                    pointBackgroundColor: forecast_gradientStroke,
                    pointHoverBackgroundColor: forecast_gradientStroke,
                    pointHoverBorderColor: forecast_gradientStroke,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: forecast_gradientFill,
                    borderWidth: 4,
                    data: [12, 2, 25, 9, 6, 17]
                }]
            },
            options: {
                legend: {
                    possition: "top",
                    labels: {
                        fontColor: "white"
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "rgba(255,255,255,1)",
                            fontStyle: "bold",
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            callback: function(label, index, labels) {
                                return label + 'k';
                            },
                            min: 0,
                            max: 30
                        },
                        gridLines: {
                            drawTicks: false,
                            display: false
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent"
                        },
                        ticks: {
                            fontColor: "rgba(255,255,255,1)",
                            fontStyle: "bold"
                        }
                    }]
                }
            }
        });
        salesChart.render();
    }
    makeVisitorChart(){
        var canvas: any = document.getElementById("visitorChart");
        var context = canvas.getContext("2d");
        canvas.height = "368px";
        var new_visitorgradientStroke = context.createLinearGradient(0,0,0,400);
        new_visitorgradientStroke.addColorStop(0,"rgba(204,0,23,0.97)");
        new_visitorgradientStroke.addColorStop(1, 'rgba(204,0,23,0.32)');

        var new_visitorgradientFill = context.createLinearGradient(0,0,0,400);
        new_visitorgradientFill.addColorStop(0, "rgba(204,0,23,0.97)");
        new_visitorgradientFill.addColorStop(1, "rgba(204,0,23,0.32)");

        var ret_visitorgradientStroke = context.createLinearGradient(0,0,0,400);
        ret_visitorgradientStroke.addColorStop(0,"rgba(30,136,229,1)");
        ret_visitorgradientStroke.addColorStop(1,"rgba(30,136,229,0.32)");

        var ret_visitorgradientFill = context.createLinearGradient(0,0,0,400);
        ret_visitorgradientFill.addColorStop(0,"rgba(30,136,229,1)");
        ret_visitorgradientFill.addColorStop(1,"rgba(30,136,229,0.32)");

        var visitorChart = new Chart(context,{
            type: 'line',
            data: {
                labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN"],
                datasets: [{
                    label: 'New Customers',
                    borderColor: new_visitorgradientStroke,
                    pointBorderColor: new_visitorgradientStroke,
                    pointBackgroundColor: new_visitorgradientStroke,
                    pointHoverBackgroundColor: new_visitorgradientStroke,
                    pointHoverBorderColor: new_visitorgradientStroke,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: new_visitorgradientFill,
                    borderWidth: 4,
                    data: [120, 230, 110, 104, 82, 237]
                },
                {
                    label: 'Returning Customers',
                    borderColor: ret_visitorgradientStroke,
                    pointBorderColor: ret_visitorgradientStroke,
                    pointBackgroundColor: ret_visitorgradientStroke,
                    pointHoverBackgroundColor: ret_visitorgradientStroke,
                    pointHoverBorderColor: ret_visitorgradientStroke,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: ret_visitorgradientFill,
                    borderWidth: 4,
                    data: [120, 205, 250, 90, 486, 170]
                }]
            },
            options: {
                legend: {
                    possition: "top"
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "rgba(30, 136, 229,1)",
                            fontStyle: "bold",
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            callback: function(label, index, labels) {
                                return label + 'k';
                            },
                            min: 0,
                            max: 500
                        },
                        gridLines: {
                            drawTicks: false,
                            display: false
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent"
                        },
                        ticks: {
                            fontColor: "rgba(204,0,23,0.97)",
                            fontStyle: "bold"
                        }
                    }]
                }
            }
        });
        visitorChart.render();
    }

    initTwitterWidget() {
        this.twitter = this._router.events.subscribe(val => {
            if (val instanceof NavigationEnd) {
                (<any>window).twttr = (function(d, s, id) {
                    let js: any, fjs = d.getElementsByTagName(s)[0],
                        t = (<any>window).twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);
                    t._e = [];
                    t.ready = function(f: any) {
                        t._e.push(f);
                    };
                    return t;
                }(document, "script", "twitter-wjs"));
                (<any>window).twttr.widgets.load();
            }
        });
    }

    public _registerCustomChartJSPlugin(): void {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function(chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function(dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }

    constructor(
        private _router: Router,
        private _dashboardservice: DashboardService
    ) {
        setInterval(() => this.initTwitterWidget(), 1000);
        this._registerCustomChartJSPlugin();
      

    }

    ngOnInit() {
        this.widgets = this._dashboardservice.widgets;
        this.makeSalesChart();
        this.makeVisitorChart();
    }

    

}
