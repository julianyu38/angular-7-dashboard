import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { DashboardComponent } from './dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule, GaugeModule } from '@swimlane/ngx-charts';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material';
import { NgCircleProgressModule } from "ng-circle-progress";
import { NgxTwitterTimelineModule } from 'ngx-twitter-timeline';
import { DashboardService } from './dashboard.service';

const routes = [
    {
        path     : 'dashboard',
        component: DashboardComponent,
        resolve : {
            data: DashboardService
        }
    }
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule,
        ChartsModule,
        NgxChartsModule,
        BrowserModule,
        MatIconModule,
        GaugeModule,
        NgCircleProgressModule.forRoot({
            radius: 100,
            outerStrokeWidth: 10,
            innerStrokeWidth: 5,
            showBackground: false,
            startFromZero: false,
            backgroundPadding: -10,
            maxPercent: 100,
            showInnerStroke: true,
            subtitle: "Uptime"
        }),
        NgxTwitterTimelineModule.forRoot()
    ],
    providers:[DashboardService],
    bootstrap:[DashboardComponent],
    exports     : [
        DashboardComponent
    ]
})

export class DashboardModule
{
}
