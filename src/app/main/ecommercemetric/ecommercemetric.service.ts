import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class WeatherService {
    _weatherxu_url: string =
        "http://api.apixu.com/v1/forecast.json?key=ef890c3af5ea4e4aafd12952182411&q=London&days=7";
    constructor(private weatherhttp: HttpClient) {}

    getWeatherNews() {
        return this.weatherhttp.get(this._weatherxu_url);
    }
}
