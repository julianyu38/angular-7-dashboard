import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { HttpClientModule } from '@angular/common/http';

import { FuseSharedModule } from "@fuse/shared.module";

import { EcommercemetricComponent } from "./ecommercemetric.component";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatTabsModule,
    MatIcon
} from "@angular/material";
import { NgCircleProgressModule } from "ng-circle-progress";

import { WeatherService } from './ecommercemetric.service';

const routes = [
    {
        path: "ecommercemetric",
        component: EcommercemetricComponent
    }
];

@NgModule({
    declarations: [EcommercemetricComponent],
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,
        HttpClientModule,

        FuseSharedModule,
        MatIconModule,
        MatSelectModule,
        MatFormFieldModule,
        NgCircleProgressModule.forRoot({
            radius: 100,
            outerStrokeWidth: 10,
            innerStrokeWidth: 5,
            showBackground: false,
            startFromZero: false,
            backgroundPadding: -10,
            maxPercent: 100,
            showInnerStroke: true,
            subtitle:"Uptime"
        })
    ],
    providers: [WeatherService],
    exports: [EcommercemetricComponent]
})
export class EcommercemetricModule {}
