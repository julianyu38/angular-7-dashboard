import { Component, OnInit } from "@angular/core"

import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service"
import { WeatherService } from "./ecommercemetric.service"

@Component({
    selector: "ecommercemetric",
    templateUrl: "./ecommercemetric.component.html",
    styleUrls: ["./ecommercemetric.component.scss"]
})
export class EcommercemetricComponent implements OnInit{
    //Progress Bar
    horizontalColorRanges1 = [{ stop: 100, color: "#aa66cc" }];
    horizontalColorRanges2 = [{ stop: 100, color: "#ff4444" }];
    horizontalColorRanges3 = [{ stop: 100, color: "#f0ad4e" }];

    //Weather News
    weather_data = {};

    current_date_status_text : string;
    current_date_status_icon : string;
    current_date_wind_mph : string;
    current_date_precip_in : string;
    current_date_pressure_in : string;
    current_date_temp : string;

    forecast_day1_date : string;
    forecast_day1_status_icon : string;
    forecast_day1_temp : string;

    forecast_day2_date : string;
    forecast_day2_status_icon : string;
    forecast_day2_temp : string;

    forecast_day3_date : string;
    forecast_day3_status_icon : string;
    forecast_day3_temp : string;

    forecast_day4_date : string;
    forecast_day4_status_icon : string;
    forecast_day4_temp : string;

    forecast_day5_date : string;
    forecast_day5_status_icon : string;
    forecast_day5_temp : string;

    forecast_day6_date : string;
    forecast_day6_status_icon : string;
    forecast_day6_temp : string;

    forecast_day7_date : string;
    forecast_day7_status_icon : string; 
    forecast_day7_temp : string;





    constructor(private weatherService: WeatherService) { }

    ngOnInit(){
        this.weatherService.getWeatherNews().subscribe(data => {
            this.weather_data = data;
            console.log("Weather", this.weather_data);
            this.current_date_status_text = this.weather_data["current"]["condition"]["text"];
            this.current_date_status_icon = this.weather_data["current"]["condition"]["icon"];
            this.current_date_wind_mph = this.weather_data["current"]["wind_mph"];
            this.current_date_precip_in = this.weather_data["current"]["precip_in"];
            this.current_date_pressure_in = this.weather_data["current"]["pressure_in"];
            this.current_date_temp = this. weather_data["current"]["temp_f"];

            this.forecast_day1_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["0"]["date"]);
            this.forecast_day2_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["1"]["date"]);
            this.forecast_day3_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["2"]["date"]);
            this.forecast_day4_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["3"]["date"]);
            this.forecast_day5_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["4"]["date"]);
            this.forecast_day6_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["5"]["date"]);
            this.forecast_day7_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["6"]["date"]);


            this.forecast_day1_status_icon = this.weather_data["forecast"]["forecastday"]["0"]["day"]["condition"]["icon"];
            this.forecast_day1_temp = this.weather_data["forecast"]["forecastday"]["0"]["day"]["avgtemp_f"];

            
            this.forecast_day2_status_icon = this.weather_data["forecast"]["forecastday"]["1"]["day"]["condition"]["icon"];
            this.forecast_day2_temp = this.weather_data["forecast"]["forecastday"]["1"]["day"]["avgtemp_f"];

            
            this.forecast_day3_status_icon = this.weather_data["forecast"]["forecastday"]["2"]["day"]["condition"]["icon"];
            this.forecast_day3_temp = this.weather_data["forecast"]["forecastday"]["2"]["day"]["avgtemp_f"];

            
            this.forecast_day4_status_icon = this.weather_data["forecast"]["forecastday"]["3"]["day"]["condition"]["icon"];
            this.forecast_day4_temp = this.weather_data["forecast"]["forecastday"]["3"]["day"]["avgtemp_f"];

            
            this.forecast_day5_status_icon = this.weather_data["forecast"]["forecastday"]["4"]["day"]["condition"]["icon"];
            this.forecast_day5_temp = this.weather_data["forecast"]["forecastday"]["4"]["day"]["avgtemp_f"];

            
            this.forecast_day6_status_icon = this.weather_data["forecast"]["forecastday"]["5"]["day"]["condition"]["icon"];
            this.forecast_day6_temp = this.weather_data["forecast"]["forecastday"]["5"]["day"]["avgtemp_f"];
            
            this.forecast_day7_status_icon = this.weather_data["forecast"]["forecastday"]["6"]["day"]["condition"]["icon"];
            this.forecast_day7_temp = this.weather_data["forecast"]["forecastday"]["6"]["day"]["avgtemp_f"];

        });
    }

    private getDayofWeek(date){
        var dayofWeek = new Date(date).getDay();
        return isNaN(dayofWeek) ? null : ['Sun','Mon','Tue','Wed', 'Thu', 'Fri', 'Sat'][dayofWeek];
    }

}
