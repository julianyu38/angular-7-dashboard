import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { FuseSharedModule } from "@fuse/shared.module";

import { EcommercemetriconeComponent } from "./ecommercemetricone.component";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatTabsModule,
    MatIcon
} from "@angular/material";
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule, GaugeModule } from '@swimlane/ngx-charts';
import { WeatherService } from '../ecommercemetric/ecommercemetric.service';

const routes = [
    {
        path: "ecommercemetricone",
        component: EcommercemetriconeComponent
    }
];

@NgModule({
    declarations: [EcommercemetriconeComponent],
    imports: [RouterModule.forChild(routes), TranslateModule, FuseSharedModule, MatIconModule, ChartsModule,
        NgxChartsModule],
    providers: [WeatherService],
    exports: [EcommercemetriconeComponent]
})
export class EcommercemetriconeModule { }
