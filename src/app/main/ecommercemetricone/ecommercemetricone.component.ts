import { Component, OnInit } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { WeatherService } from "../ecommercemetric/ecommercemetric.service"



@Component({
    selector: 'ecommercemetricone',
    templateUrl: './ecommercemetricone.component.html',
    styleUrls: ['./ecommercemetricone.component.scss']
})
export class EcommercemetriconeComponent implements OnInit{
    //Widget 1 && 4 Data...
    widgets = {
        widget1: {
            chartType: 'line',
            datasets: {
                '2018': [
                    {
                        label: 'Sales',
                        data: [3.9, 2.5, 3.8, 4.1, 1.9, 3],
                        fill: 'start'

                    }
                ]

            },
            labels: ['Monday', 'Tuesday', 'Wendesday', 'Thursday', 'Friday', 'Sundeay'],
            colors: [
                {
                    // fillColor: 'rgba(151,187,205,0.2)',
                    // strokeColor: 'rgba(151,187,205,1)',
                    // pointColor: 'rgba(151,187,205,1)',
                    // pointStrokeColor:'#fff',
                    // pointHighlightFill:'#fff',
                    // pointHighlightStroke: 'rgba(151,187,205,0.8)'
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5',
                    pointBackgroundColor: '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks: {
                                min: 0,
                                max: 5,
                                stepSize: 1,
                                callback: function(label,index,labels){
                                    return label + 'k';
                                },
                                fontColor: '#ffffff'
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false
                            },
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widget2: {
            conversion: {
                value: 492,
                ofTarget: 13
            },
            chartType: 'bar',
            datasets: [
                {
                    label: 'Conversion',
                    data: [221, 428, 492, 471, 413, 344, 294]
                }
            ],
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 100,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget3: {
            impressions: {
                value: '87k',
                ofTarget: 12
            },
            chartType: 'line',
            datasets: [
                {
                    label: 'Impression',
                    data: [67000, 54000, 82000, 57000, 72000, 57000, 87000, 72000, 89000, 98700, 112000, 136000, 110000, 149000, 98000],
                    fill: false
                }
            ],
            labels: ['Jan 1', 'Jan 2', 'Jan 3', 'Jan 4', 'Jan 5', 'Jan 6', 'Jan 7', 'Jan 8', 'Jan 9', 'Jan 10', 'Jan 11', 'Jan 12', 'Jan 13', 'Jan 14', 'Jan 15'],
            colors: [
                {
                    borderColor: '#5c84f1'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                elements: {
                    point: {
                        radius: 2,
                        borderWidth: 1,
                        hoverRadius: 2,
                        hoverBorderWidth: 1
                    },
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                // min: 100,
                                // max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget4: {
            visits: {
                value: 882,
                ofTarget: -9
            },
            chartType: 'bar',
            datasets: [
                {
                    labewidget4l: 'Visits',
                    data: [4.32, 4.28, 3.27, 3.63, 4.56, 2.67, 2.31,1.45,1.89,4.21,3.98,2.23]
                }
            ],
            labels: ['9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm','4pm', '5pm', '6pm','7pm','8pm'],
            colors: [
                {
                    borderColor: '#f44336',
                    backgroundColor: '#f44336'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            gridLines:{
                                display: false
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks: {
                                min: 0,
                                max: 5,
                                callback:function(label,index,labels){
                                    return label + 'k';
                                }
                            },
                            gridLines:{
                                display:false
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: 'line',
            datasets: {
                'yesterday': [
                    {
                        label: 'Visitors',
                        data: [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill: 'start'

                    },
                    {
                        label: 'Page views',
                        data: [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
                        fill: 'start'
                    }
                ],
                'today': [
                    {
                        label: 'Visitors',
                        data: [410, 380, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
                        fill: 'start'
                    },
                    {
                        label: 'Page Views',
                        data: [3000, 3400, 4100, 3800, 2200, 3200, 2900, 1900, 2900, 3900, 2500, 3800],
                        fill: 'start'

                    }
                ]
            },
            labels: ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
            colors: [
                {
                    borderColor: '#3949ab',
                    backgroundColor: '#3949ab',
                    pointBackgroundColor: '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: 'rgba(30, 136, 229, 0.87)',
                    backgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 1000
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        },
        widget6: {
            markers: [
                {
                    lat: 52,
                    lng: -73,
                    label: '120'
                },
                {
                    lat: 37,
                    lng: -104,
                    label: '498'
                },
                {
                    lat: 21,
                    lng: -7,
                    label: '443'
                },
                {
                    lat: 55,
                    lng: 75,
                    label: '332'
                },
                {
                    lat: 51,
                    lng: 7,
                    label: '50'
                },
                {
                    lat: 31,
                    lng: 12,
                    label: '221'
                },
                {
                    lat: 45,
                    lng: 44,
                    label: '455'
                },
                {
                    lat: -26,
                    lng: 134,
                    label: '231'
                },
                {
                    lat: -9,
                    lng: -60,
                    label: '67'
                },
                {
                    lat: 33,
                    lng: 104,
                    label: '665'
                }
            ],
            styles: [
                {
                    'featureType': 'administrative',
                    'elementType': 'labels.text.fill',
                    'stylers': [
                        {
                            'color': '#444444'
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#f2f2f2'
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'road',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'saturation': -100
                        },
                        {
                            'lightness': 45
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'simplified'
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'labels.icon',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#039be5'
                        },
                        {
                            'visibility': 'on'
                        }
                    ]
                }
            ]
        },
        widget7: {
            scheme: {
                domain: ['#4867d2', '#5c84f1', '#89a9f4']
            },
            devices: [
                {
                    name: 'Desktop',
                    value: 92.8,
                    change: -0.6
                },
                {
                    name: 'Mobile',
                    value: 6.1,
                    change: 0.7
                },
                {
                    name: 'Tablet',
                    value: 1.1,
                    change: 0.1
                }
            ]
        },
        widget8: {
            scheme: {
                domain: ['#5c84f1']
            },
            today: '12,540',
            change: {
                value: 321,
                percentage: 2.05
            },
            data: [
                {
                    name: 'Sales',
                    series: [
                        {
                            name: 'Jan 1',
                            value: 540
                        },
                        {
                            name: 'Jan 2',
                            value: 539
                        },
                        {
                            name: 'Jan 3',
                            value: 538
                        },
                        {
                            name: 'Jan 4',
                            value: 539
                        },
                        {
                            name: 'Jan 5',
                            value: 540
                        },
                        {
                            name: 'Jan 6',
                            value: 539
                        },
                        {
                            name: 'Jan 7',
                            value: 540
                        }
                    ]
                }
            ],
            dataMin: 538,
            dataMax: 541
        },
        widget9: {
            rows: [
                {
                    title: 'Holiday Travel',
                    clicks: 3621,
                    conversion: 90
                },
                {
                    title: 'Get Away Deals',
                    clicks: 703,
                    conversion: 7
                },
                {
                    title: 'Airfare',
                    clicks: 532,
                    conversion: 0
                },
                {
                    title: 'Vacation',
                    clicks: 201,
                    conversion: 8
                },
                {
                    title: 'Hotels',
                    clicks: 94,
                    conversion: 4
                }
            ]
        },
        widget10: {
            visits: {
                value: 882,
                ofTarget: -9
            },
            chartType: 'bar',
            datasets: [
                {
                    labewidget4l: 'Visits',
                    data: [82,104,67,24,178,193,59]
                }
            ],
            labels: ['MON','TUES','WEN','THU','FRI','SAT','SUN'],
            colors: [
                {
                    borderColor: '#f44336',
                    backgroundColor: '#f44336'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            gridLines:{
                                display: false
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks: {
                                min: 0,
                                max: 200,
                                step:10,
                                callback:function(label,index,labels){
                                    return label;
                                }
                            },
                            gridLines:{
                                display:false
                            }
                        }
                    ]
                }
            }
        }
    };
    widget1SelectedYear = '2018';
    widget5SelectedDay = 'today';

    //jqxChart

    source: any = [
        { Browser: "Male", Share: 4250 },
        { Browser: "Gender Neutral", Share: 3970 },
        { Browser: "Under 25s", Share: 3454 },
        { Browser: "Female", Share: 2390 }
    ];
    dataAdapter: any = new jqx.dataAdapter(this.source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.source.url + '" : ' + error);
        }
    });
    padding1: any = { left: 5, top: 25, right: 5, bottom: 5 };
    titlePadding1: any = { left: 0, top: 20, right: 0, bottom: 10 };
    getWidth1(): any {
        if (document.body.offsetWidth < 300) {
            return "90%";
        }
        return "100%";
    }
    legendLayout: any = { left: 175, top: 220, width: 300, height: 300, flow: 'vertical' };

    seriesGroups1: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 60,
                    initialAngle: 300,
                    radius: 90,
                    innerRadius: 35,
                    centerOffset: 0,
                    offsetY: 90,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source.length; index++) {
                            
                            sum += this.source[index].Share;
                        }

                        console.log(sum);
                        
                        return value + ": "+ (this.source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    

    //Weather News
    weather_data = {};

    current_date_status_text : string;
    current_date_status_icon : string;
    current_date_wind_mph : string;
    current_date_precip_in : string;
    current_date_pressure_in : string;
    current_date_temp : string;

    forecast_day1_date : string;
    forecast_day1_status_icon : string;
    forecast_day1_temp : string;

    forecast_day2_date : string;
    forecast_day2_status_icon : string;
    forecast_day2_temp : string;

    forecast_day3_date : string;
    forecast_day3_status_icon : string;
    forecast_day3_temp : string;

    forecast_day4_date : string;
    forecast_day4_status_icon : string;
    forecast_day4_temp : string;

    forecast_day5_date : string;
    forecast_day5_status_icon : string;
    forecast_day5_temp : string;

    forecast_day6_date : string;
    forecast_day6_status_icon : string;
    forecast_day6_temp : string;

    forecast_day7_date : string;
    forecast_day7_status_icon : string; 
    forecast_day7_temp : string;

    constructor(private weatherService: WeatherService
    ) {
        this._registerCustomChartJSPlugin();
    }

    ngOnInit(){
        this.weatherService.getWeatherNews().subscribe(data => {
            this.weather_data = data;
            console.log("Weather", this.weather_data);
            this.current_date_status_text = this.weather_data["current"]["condition"]["text"];
            this.current_date_status_icon = this.weather_data["current"]["condition"]["icon"];
            this.current_date_wind_mph = this.weather_data["current"]["wind_mph"];
            this.current_date_precip_in = this.weather_data["current"]["precip_in"];
            this.current_date_pressure_in = this.weather_data["current"]["pressure_in"];
            this.current_date_temp = this. weather_data["current"]["temp_f"];

            this.forecast_day1_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["0"]["date"]);
            this.forecast_day2_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["1"]["date"]);
            this.forecast_day3_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["2"]["date"]);
            this.forecast_day4_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["3"]["date"]);
            this.forecast_day5_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["4"]["date"]);
            this.forecast_day6_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["5"]["date"]);
            this.forecast_day7_date = this.getDayofWeek(this.weather_data["forecast"]["forecastday"]["6"]["date"]);


            this.forecast_day1_status_icon = this.weather_data["forecast"]["forecastday"]["0"]["day"]["condition"]["icon"];
            this.forecast_day1_temp = this.weather_data["forecast"]["forecastday"]["0"]["day"]["avgtemp_f"];

            
            this.forecast_day2_status_icon = this.weather_data["forecast"]["forecastday"]["1"]["day"]["condition"]["icon"];
            this.forecast_day2_temp = this.weather_data["forecast"]["forecastday"]["1"]["day"]["avgtemp_f"];

            
            this.forecast_day3_status_icon = this.weather_data["forecast"]["forecastday"]["2"]["day"]["condition"]["icon"];
            this.forecast_day3_temp = this.weather_data["forecast"]["forecastday"]["2"]["day"]["avgtemp_f"];

            
            this.forecast_day4_status_icon = this.weather_data["forecast"]["forecastday"]["3"]["day"]["condition"]["icon"];
            this.forecast_day4_temp = this.weather_data["forecast"]["forecastday"]["3"]["day"]["avgtemp_f"];

            
            this.forecast_day5_status_icon = this.weather_data["forecast"]["forecastday"]["4"]["day"]["condition"]["icon"];
            this.forecast_day5_temp = this.weather_data["forecast"]["forecastday"]["4"]["day"]["avgtemp_f"];

            
            this.forecast_day6_status_icon = this.weather_data["forecast"]["forecastday"]["5"]["day"]["condition"]["icon"];
            this.forecast_day6_temp = this.weather_data["forecast"]["forecastday"]["5"]["day"]["avgtemp_f"];
            
            this.forecast_day7_status_icon = this.weather_data["forecast"]["forecastday"]["6"]["day"]["condition"]["icon"];
            this.forecast_day7_temp = this.weather_data["forecast"]["forecastday"]["6"]["day"]["avgtemp_f"];

        });
    }
    
    private getDayofWeek(date){
        var dayofWeek = new Date(date).getDay();
        return isNaN(dayofWeek) ? null : ['Sun','Mon','Tue','Wed', 'Thu', 'Fri', 'Sat'][dayofWeek];
    }

    private _registerCustomChartJSPlugin(): void {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }
}
