import { Component, OnInit,ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import * as jsPDF from 'jspdf';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import * as CanvasJS from 'app/canvasjs.min';
import { jqxBarGaugeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge';
import { jqxGaugeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgauge';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { ChartCommonModule } from '@swimlane/ngx-charts';
import { Chart } from 'chart.js';


@Component({
    selector   : 'reporting',
    templateUrl: './reporting.component.html',
    styleUrls  : ['./reporting.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ReportingComponent implements OnInit
{

    widgets = {
        widget1: {
            chartType: 'line',
            datasets: {
                '2016': [
                    {
                        label: 'Sales',
                        data: [1.9, 3, 3.4, 2.2, 2.9, 3.9, 2.5, 3.8, 4.1, 3.8, 3.2, 2.9],
                        fill: 'start'

                    }
                ],
                '2017': [
                    {
                        label: 'Sales',
                        data: [2.2, 2.9, 3.9, 2.5, 3.8, 3.2, 2.9, 1.9, 3, 3.4, 4.1, 3.8],
                        fill: 'start'

                    }
                ],
                '2018': [
                    {
                        label: 'Sales',
                        data: [3.9, 2.5, 3.8, 4.1, 1.9, 3, 3.8, 3.2, 2.9, 3.4, 2.2, 2.9],
                        fill: 'start'

                    }
                ]

            },
            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            colors: [
                {
                    // fillColor: 'rgba(151,187,205,0.2)',
                    // strokeColor: 'rgba(151,187,205,1)',
                    // pointColor: 'rgba(151,187,205,1)',
                    // pointStrokeColor:'#fff',
                    // pointHighlightFill:'#fff',
                    // pointHighlightStroke: 'rgba(151,187,205,0.8)'
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5',
                    pointBackgroundColor: '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 1.5,
                                max: 5,
                                stepSize: 0.5
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widget2: {
            conversion: {
                value: 37.6,
                ofTarget: 13
            },
            chartType: 'bar',
            datasets: [
                {
                    label: 'Conversion',
                    data: [1.9, 3, 3.4, 2.2, 2.9, 3.9, 2.5, 3.8, 4.1, 3.8, 3.2, 2.9]
                }
            ],
            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 0,
                                max: 5
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: 'line',
            datasets: {
                'yesterday': [
                    {
                        label: 'Visitors',
                        data: [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill: 'start'

                    },
                    {
                        label: 'Page views',
                        data: [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
                        fill: 'start'
                    }
                ],
                'today': [
                    {
                        label: 'Visitors',
                        data: [320, 280, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
                        fill: 'start'
                    },
                    {
                        label: 'Page Views',
                        data: [300, 340, 410, 380, 220, 320, 290, 190, 290, 390, 250, 380],
                        fill: 'start'

                    }
                ]
            },
            labels: ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
            colors: [
                {
                    borderColor: '#3949ab',
                    backgroundColor: 'rgba(30, 136, 229, 0)',
                    pointBackgroundColor: '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: 'rgba(30, 136, 229, 0.87)',
                    backgroundColor: 'rgba(30, 136, 229, 0)',
                    pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 100
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        }
    };
    widgets1 = {
        widget1: {
            chartType: "line",
            datasets: {
                "2016": [
                    {
                        label: "Sales",
                        data: [
                            1.9,
                            3,
                            3.4,
                            2.2,
                            2.9,
                            3.9,
                            2.5,
                            3.8,
                            4.1,
                            3.8,
                            3.2,
                            2.9
                        ],
                        fill: "start"
                    }
                ],
                "2017": [
                    {
                        label: "Sales",
                        data: [
                            2.2,
                            2.9,
                            3.9,
                            2.5,
                            3.8,
                            3.2,
                            2.9,
                            1.9,
                            3,
                            3.4,
                            4.1,
                            3.8
                        ],
                        fill: "start"
                    }
                ],
                "2018": [
                    {
                        label: "Sales",
                        data: [
                            3.9,
                            2.5,
                            3.8,
                            4.1,
                            1.9,
                            3,
                            3.8,
                            3.2,
                            2.9,
                            3.4,
                            2.2,
                            2.9
                        ],
                        fill: "start"
                    }
                ]
            },
            labels: [
                "JAN",
                "FEB",
                "MAR",
                "APR",
                "MAY",
                "JUN",
                "JUL",
                "AUG",
                "SEP",
                "OCT",
                "NOV",
                "DEC"
            ],
            colors: [
                {
                    borderColor: "#42a5f5",
                    backgroundColor: "#42a5f5",
                    pointBackgroundColor: "#1e88e5",
                    pointHoverBackgroundColor: "#1e88e5",
                    pointBorderColor: "#ffffff",
                    pointHoverBorderColor: "#ffffff"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: "#ffffff"
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 1.5,
                                max: 5,
                                stepSize: 0.5
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widget2: {
            conversion: {
                value: 492,
                ofTarget: 13
            },
            chartType: "bar",
            datasets: [
                {
                    label: "Conversion",
                    data: [221, 428, 492, 471, 413, 344, 294]
                }
            ],
            labels: [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
            ],
            colors: [
                {
                    borderColor: "#42a5f5",
                    backgroundColor: "#42a5f5"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 100,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget3: {
            impressions: {
                value: "87k",
                ofTarget: 12
            },
            chartType: "line",
            datasets: [
                {
                    label: "Impression",
                    data: [
                        67000,
                        54000,
                        82000,
                        57000,
                        72000,
                        57000,
                        87000,
                        72000,
                        89000,
                        98700,
                        112000,
                        136000,
                        110000,
                        149000,
                        98000
                    ],
                    fill: false
                }
            ],
            labels: [
                "Jan 1",
                "Jan 2",
                "Jan 3",
                "Jan 4",
                "Jan 5",
                "Jan 6",
                "Jan 7",
                "Jan 8",
                "Jan 9",
                "Jan 10",
                "Jan 11",
                "Jan 12",
                "Jan 13",
                "Jan 14",
                "Jan 15"
            ],
            colors: [
                {
                    borderColor: "#5c84f1"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                elements: {
                    point: {
                        radius: 2,
                        borderWidth: 1,
                        hoverRadius: 2,
                        hoverBorderWidth: 1
                    },
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                // min: 100,
                                // max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget4: {
            visits: {
                value: 882,
                ofTarget: -9
            },
            chartType: "bar",
            datasets: [
                {
                    label: "Visits",
                    data: [32, 28, 27, 63, 56, 67]
                }
            ],
            labels: [
                "JAN",
                "FEB",
                "MAR",
                "APR",
                "MAY",
                "JUN"
                
            ],
            colors: [
                {
                    borderColor: "#f44336",
                    backgroundColor: "#f44336"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            gridLines:{
                                display: false
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks: {
                                min: 0,
                                max: 100,
                                step:20
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: "line",
            datasets: {
                yesterday: [
                    {
                        label: "Visitors",
                        data: [
                            190,
                            300,
                            340,
                            220,
                            290,
                            390,
                            250,
                            380,
                            410,
                            380,
                            320,
                            290
                        ],
                        fill: "start"
                    },
                    {
                        label: "Page views",
                        data: [
                            2200,
                            2900,
                            3900,
                            2500,
                            3800,
                            3200,
                            2900,
                            1900,
                            3000,
                            3400,
                            4100,
                            3800
                        ],
                        fill: "start"
                    }
                ],
                today: [
                    {
                        label: "Visitors",
                        data: [
                            410,
                            380,
                            320,
                            290,
                            190,
                            390,
                            250,
                            380,
                            300,
                            340,
                            220,
                            290
                        ],
                        fill: "start"
                    },
                    {
                        label: "Page Views",
                        data: [
                            3000,
                            3400,
                            4100,
                            3800,
                            2200,
                            3200,
                            2900,
                            1900,
                            2900,
                            3900,
                            2500,
                            3800
                        ],
                        fill: "start"
                    }
                ]
            },
            labels: [
                "12am",
                "2am",
                "4am",
                "6am",
                "8am",
                "10am",
                "12pm",
                "2pm",
                "4pm",
                "6pm",
                "8pm",
                "10pm"
            ],
            colors: [
                {
                    borderColor: "#3949ab",
                    backgroundColor: "#3949ab",
                    pointBackgroundColor: "#3949ab",
                    pointHoverBackgroundColor: "#3949ab",
                    pointBorderColor: "#ffffff",
                    pointHoverBorderColor: "#ffffff"
                },
                {
                    borderColor: "rgba(30, 136, 229, 0.87)",
                    backgroundColor: "rgba(30, 136, 229, 0.87)",
                    pointBackgroundColor: "rgba(30, 136, 229, 0.87)",
                    pointHoverBackgroundColor: "rgba(30, 136, 229, 0.87)",
                    pointBorderColor: "#ffffff",
                    pointHoverBorderColor: "#ffffff"
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: "nearest",
                    mode: "index",
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontColor: "rgba(0,0,0,0.54)"
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 1000
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        },
        widget6: {
            markers: [
                {
                    lat: 52,
                    lng: -73,
                    label: "120"
                },
                {
                    lat: 37,
                    lng: -104,
                    label: "498"
                },
                {
                    lat: 21,
                    lng: -7,
                    label: "443"
                },
                {
                    lat: 55,
                    lng: 75,
                    label: "332"
                },
                {
                    lat: 51,
                    lng: 7,
                    label: "50"
                },
                {
                    lat: 31,
                    lng: 12,
                    label: "221"
                },
                {
                    lat: 45,
                    lng: 44,
                    label: "455"
                },
                {
                    lat: -26,
                    lng: 134,
                    label: "231"
                },
                {
                    lat: -9,
                    lng: -60,
                    label: "67"
                },
                {
                    lat: 33,
                    lng: 104,
                    label: "665"
                }
            ],
            styles: [
                {
                    featureType: "administrative",
                    elementType: "labels.text.fill",
                    stylers: [
                        {
                            color: "#444444"
                        }
                    ]
                },
                {
                    featureType: "landscape",
                    elementType: "all",
                    stylers: [
                        {
                            color: "#f2f2f2"
                        }
                    ]
                },
                {
                    featureType: "poi",
                    elementType: "all",
                    stylers: [
                        {
                            visibility: "off"
                        }
                    ]
                },
                {
                    featureType: "road",
                    elementType: "all",
                    stylers: [
                        {
                            saturation: -100
                        },
                        {
                            lightness: 45
                        }
                    ]
                },
                {
                    featureType: "road.highway",
                    elementType: "all",
                    stylers: [
                        {
                            visibility: "simplified"
                        }
                    ]
                },
                {
                    featureType: "road.arterial",
                    elementType: "labels.icon",
                    stylers: [
                        {
                            visibility: "off"
                        }
                    ]
                },
                {
                    featureType: "transit",
                    elementType: "all",
                    stylers: [
                        {
                            visibility: "off"
                        }
                    ]
                },
                {
                    featureType: "water",
                    elementType: "all",
                    stylers: [
                        {
                            color: "#039be5"
                        },
                        {
                            visibility: "on"
                        }
                    ]
                }
            ]
        },
        widget7: {
            scheme: {
                domain: ["#4867d2", "#5c84f1", "#89a9f4"]
            },
            devices: [
                {
                    name: "Desktop",
                    value: 92.8,
                    change: -0.6
                },
                {
                    name: "Mobile",
                    value: 6.1,
                    change: 0.7
                },
                {
                    name: "Tablet",
                    value: 1.1,
                    change: 0.1
                }
            ]
        },
        widget8: {
            scheme: {
                domain: ["#5c84f1"]
            },
            today: "12,540",
            change: {
                value: 321,
                percentage: 2.05
            },
            data: [
                {
                    name: "Sales",
                    series: [
                        {
                            name: "Jan 1",
                            value: 540
                        },
                        {
                            name: "Jan 2",
                            value: 539
                        },
                        {
                            name: "Jan 3",
                            value: 538
                        },
                        {
                            name: "Jan 4",
                            value: 539
                        },
                        {
                            name: "Jan 5",
                            value: 540
                        },
                        {
                            name: "Jan 6",
                            value: 539
                        },
                        {
                            name: "Jan 7",
                            value: 540
                        }
                    ]
                }
            ],
            dataMin: 538,
            dataMax: 541
        },
        widget9: {
            rows: [
                {
                    title: "Holiday Travel",
                    clicks: 3621,
                    conversion: 90
                },
                {
                    title: "Get Away Deals",
                    clicks: 703,
                    conversion: 7
                },
                {
                    title: "Airfare",
                    clicks: 532,
                    conversion: 0
                },
                {
                    title: "Vacation",
                    clicks: 201,
                    conversion: 8
                },
                {
                    title: "Hotels",
                    clicks: 94,
                    conversion: 4
                }
            ]
        }
    };

    service_widgets = {
        widget1: {
            chartType: 'line',
            datasets : {
                '2016': [
                    {
                        label: 'Sales',
                        data : [78, 90, 85, 32, 29, 59],
                        fill : 'start'

                    }
                ],
                '2017': [
                    {
                        label: 'Sales',
                        data : [2.2, 2.9, 3.9, 2.5, 3.8, 3.2, 2.9, 1.9, 3, 3.4, 4.1, 3.8],
                        fill : 'start'

                    }
                ],
                '2018': [
                    {
                        label: 'Sales',
                        data : [3.9, 2.5, 3.8, 4.1, 1.9, 3, 3.8, 3.2, 2.9, 3.4, 2.2, 2.9],
                        fill : 'start'

                    }
                ]

            },
            
            labels   : ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN'],
            colors   : [
                {
                    borderColor              : '#42a5f5',
                    backgroundColor          : '#42a5f5',
                    pointBackgroundColor     : '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor         : '#ffffff',
                    pointHoverBorderColor    : '#ffffff'
                }
            ],
            options  : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                layout             : {
                    padding: {
                        top  : 32,
                        left : 32,
                        right: 32
                    }
                },
                elements           : {
                    point: {
                        radius          : 4,
                        borderWidth     : 2,
                        hoverRadius     : 4,
                        hoverBorderWidth: 2
                    },
                    line : {
                        tension: 0.4
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            gridLines: {
                                display       : false,
                                drawBorder    : false,
                                tickMarkLength: 18
                            },
                            ticks    : {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks  : {
                                min     : 0,
                                max     : 100,
                                stepSize: 10,
                                fontColor: '#ffffff',
                                callback: function(label,index,labels){
                                    return label + '%';
                                },
                            },
                            gridLines: {
                                display:false,
                                drawBorder : false,
                                tickMarkLength: 18
                            }
                        }
                    ]
                },
                plugins            : {
                    filler      : {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widget2: {
            conversion: {
                value   : 492,
                ofTarget: 13
            },
            chartType : 'bar',
            datasets  : [
                {
                    label: 'Conversion',
                    data : [221, 428, 492, 471, 413, 344, 294]
                }
            ],
            labels    : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors    : [
                {
                    borderColor    : '#42a5f5',
                    backgroundColor: '#42a5f5'
                }
            ],
            options   : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                layout             : {
                    padding: {
                        top   : 24,
                        left  : 16,
                        right : 16,
                        bottom: 16
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks  : {
                                min: 100,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget3: {
            impressions: {
                value   : '87k',
                ofTarget: 12
            },
            chartType  : 'line',
            datasets   : [
                {
                    label: 'Impression',
                    data : [67000, 54000, 82000, 57000, 72000, 57000, 87000, 72000, 89000, 98700, 112000, 136000, 110000, 149000, 98000],
                    fill : false
                }
            ],
            labels     : ['Jan 1', 'Jan 2', 'Jan 3', 'Jan 4', 'Jan 5', 'Jan 6', 'Jan 7', 'Jan 8', 'Jan 9', 'Jan 10', 'Jan 11', 'Jan 12', 'Jan 13', 'Jan 14', 'Jan 15'],
            colors     : [
                {
                    borderColor: '#5c84f1'
                }
            ],
            options    : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                elements           : {
                    point: {
                        radius          : 2,
                        borderWidth     : 1,
                        hoverRadius     : 2,
                        hoverBorderWidth: 1
                    },
                    line : {
                        tension: 0
                    }
                },
                layout             : {
                    padding: {
                        top   : 24,
                        left  : 16,
                        right : 16,
                        bottom: 16
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks  : {
                                // min: 100,
                                // max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget4: {
            visits   : {
                value   : 882,
                ofTarget: -9
            },
            chartType: 'bar',
            datasets : [
                {
                    label: 'Visits',
                    data : [432, 428, 327, 363, 456, 267, 231]
                }
            ],
            labels   : ['JAN', 'FEB', 'MAR', 'APR', 'JUN', 'JUL'],
            colors   : [
                {
                    borderColor    : '#f44336',
                    backgroundColor: '#f44336'
                }
            ],
            options  : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                layout             : {
                    padding: {
                        top   : 24,
                        left  : 16,
                        right : 16,
                        bottom: 16
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            display: true
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks  : {
                                min: 150,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: 'line',
            datasets : {
                'yesterday': [
                    {
                        label: 'Visitors',
                        data : [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill : 'start'

                    },
                    {
                        label: 'Page views',
                        data : [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
                        fill : 'start'
                    }
                ],
                'today'    : [
                    {
                        label: 'Visitors',
                        data : [3.2, 3.8, 3.2, 2.9, 1.9, 3.9, 2.5, 3.8, 3, 3.4, 2.2, 2.9],
                        fill : 'start'
                    },
                    {
                        label: 'Page Views',
                        data : [3, 3.4, 4.1, 3.8, 2.2, 3.2, 2.9, 1.9, 2.9, 3.9, 2.5, 3.8],
                        fill : 'start'

                    }
                ]
            },
            labels   : ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
            colors   : [
                {
                    borderColor              : '#3949ab',
                    backgroundColor          : '#3949ab',
                    pointBackgroundColor     : '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor         : '#ffffff',
                    pointHoverBorderColor    : '#ffffff'
                },
                {
                    borderColor              : 'rgba(30, 136, 229, 0.87)',
                    backgroundColor          : 'rgba(30, 136, 229, 0.87)',
                    pointBackgroundColor     : 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor         : '#ffffff',
                    pointHoverBorderColor    : '#ffffff'
                }
            ],
            options  : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips           : {
                    position : 'nearest',
                    mode     : 'index',
                    intersect: false
                },
                layout             : {
                    padding: {
                        left : 24,
                        right: 32
                    }
                },
                elements           : {
                    point: {
                        radius          : 4,
                        borderWidth     : 2,
                        hoverRadius     : 4,
                        hoverBorderWidth: 2
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks    : {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks    : {
                                min:1,
                                max:5,
                                stepSize: 1,
                                callback: function(label,index,labels){
                                    return label;
                                }
                            },
                            
                        }
                    ]
                },
                plugins            : {
                    filler: {
                        propagate: false
                    }
                }
            }
        },
        widget6: {
            markers: [
                {
                    lat  : 52,
                    lng  : -73,
                    label: '120'
                },
                {
                    lat  : 37,
                    lng  : -104,
                    label: '498'
                },
                {
                    lat  : 21,
                    lng  : -7,
                    label: '443'
                },
                {
                    lat  : 55,
                    lng  : 75,
                    label: '332'
                },
                {
                    lat  : 51,
                    lng  : 7,
                    label: '50'
                },
                {
                    lat  : 31,
                    lng  : 12,
                    label: '221'
                },
                {
                    lat  : 45,
                    lng  : 44,
                    label: '455'
                },
                {
                    lat  : -26,
                    lng  : 134,
                    label: '231'
                },
                {
                    lat  : -9,
                    lng  : -60,
                    label: '67'
                },
                {
                    lat  : 33,
                    lng  : 104,
                    label: '665'
                }
            ],
            styles : [
                {
                    'featureType': 'administrative',
                    'elementType': 'labels.text.fill',
                    'stylers'    : [
                        {
                            'color': '#444444'
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'color': '#f2f2f2'
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'road',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'saturation': -100
                        },
                        {
                            'lightness': 45
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'visibility': 'simplified'
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'labels.icon',
                    'stylers'    : [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'color': '#039be5'
                        },
                        {
                            'visibility': 'on'
                        }
                    ]
                }
            ]
        },
        widget7: {
            scheme : {
                domain: ['#4867d2', '#5c84f1', '#89a9f4']
            },
            devices: [
                {
                    name  : 'Desktop',
                    value : 92.8,
                    change: -0.6
                },
                {
                    name  : 'Mobile',
                    value : 6.1,
                    change: 0.7
                },
                {
                    name  : 'Tablet',
                    value : 1.1,
                    change: 0.1
                }
            ]
        },
        widget8: {
            scheme : {
                domain: ['#5c84f1']
            },
            today  : '12,540',
            change : {
                value     : 321,
                percentage: 2.05
            },
            data   : [
                {
                    name  : 'Sales',
                    series: [
                        {
                            name : 'Jan 1',
                            value: 540
                        },
                        {
                            name : 'Jan 2',
                            value: 539
                        },
                        {
                            name : 'Jan 3',
                            value: 538
                        },
                        {
                            name : 'Jan 4',
                            value: 539
                        },
                        {
                            name : 'Jan 5',
                            value: 540
                        },
                        {
                            name : 'Jan 6',
                            value: 539
                        },
                        {
                            name : 'Jan 7',
                            value: 540
                        }
                    ]
                }
            ],
            dataMin: 538,
            dataMax: 541
        },
        widget9: {
            rows: [
                {
                    title     : 'Holiday Travel',
                    clicks    : 3621,
                    conversion: 90
                },
                {
                    title     : 'Get Away Deals',
                    clicks    : 703,
                    conversion: 7
                },
                {
                    title     : 'Airfare',
                    clicks    : 532,
                    conversion: 0
                },
                {
                    title     : 'Vacation',
                    clicks    : 201,
                    conversion: 8
                },
                {
                    title     : 'Hotels',
                    clicks    : 94,
                    conversion: 4
                }
            ]
        }
    };
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';

    //service_table

    service_table_columns: any[] =
    [
        { text: '#', dataField: '#', width:'10%', align:'center', cellsAlign: 'center' },
        { text: 'Server', dataField: 'Server', width: '20%', align:'center', cellsAlign: 'center' },
        { text: 'Description', dataField: 'Description', width:'30%', align: 'center', cellsAlign: 'center' },
        { text: 'Time to Fix', dataField: 'Time to Fix', width: '20%', align: 'center', cellsAlign: 'center' },
        { text: 'Criticality', dataField: 'Criticality', width: '20%', align: 'center', cellsAlign: 'center' }
    ];

    // gsc chart
    source: any = [
        { Browser: "Male", Share: 4250 },
        { Browser: "Gender Neutral", Share: 3970 },
        { Browser: "Under 25s", Share: 3454 },
        { Browser: "Female", Share: 2390 }
    ];
    dataAdapter: any = new jqx.dataAdapter(this.source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.source.url + '" : ' + error);
        }
    });
    padding1: any = { left: 5, top: 25, right: 5, bottom: 5 };
    titlePadding1: any = { left: 0, top: 20, right: 0, bottom: 10 };
    getWidth1(): any {
        if (document.body.offsetWidth < 300) {
            return "90%";
        }
        return "100%";
    }
    legendLayout: any = { left: 150, top: 220, width: 300, height: 300, flow: 'vertical' };

    seriesGroups1: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 60,
                    initialAngle: 300,
                    radius: 90,
                    innerRadius: 35,
                    centerOffset: 0,
                    offsetY: 90,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source.length; index++) {
                            
                            sum += this.source[index].Share;
                        }

                        console.log(sum);
                        
                        return value + ": "+ (this.source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];
    

    // nps segment
    source_donut1: any = [
        { Browser: "Generation Z", Share: 4250 },
        { Browser: "Over 50s", Share: 3970 },
        { Browser: "Under 25s", Share: 3454 },
        { Browser: "26-49 ", Share: 2390 }
    ];
    dataAdapter_donut1: any = new jqx.dataAdapter(this.source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.source.url + '" : ' + error);
        }
    });
    padding_donut1: any = { left: 5, top: 25, right: 5, bottom: 5 };
    titlePadding_donut1: any = { left: 0, top: 20, right: 0, bottom: 10 };
    getWidth1_donut1(): any {
        if (document.body.offsetWidth < 300) {
            return "90%";
        }
        return "100%";
    }
    seriesGroups_donut1: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 48,
                    initialAngle: 300,
                    radius: 80,
                    innerRadius:30,
                    centerOffset: 0,
                    offsetY: 80,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    formatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source.length; index++) {
                            sum += this.source[index].Share;
                        }

                        console.log(sum);
                        
                        return (this.source[itemIndex].Share/sum * 100) .toFixed(1)+ "%";
                    },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source.length; index++) {
                            sum += this.source[index].Share;
                        }

                        console.log(sum);
                        
                        return value + ": "+ (this.source_donut1[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    legendLayout_donut1: any = {
        left: 200,
        top: 180,
        width: 200,
        height: 240,
        flow: "vertical"
    };

    source_donut2: any = [
        { Browser: "Homepage Only", Share: 4250 },
        { Browser: "Home & Shop", Share: 3970 },
        { Browser: "Shop", Share: 3454 },
        { Browser: "Basket & Exit", Share: 2390 }
    ];
    dataAdapter_donut2: any = new jqx.dataAdapter(this.source_donut2, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.source_donut2.url + '" : ' + error);
        }
    });
    padding_donut2: any = { left: 5, top: 25, right: 5, bottom: 5 };
    titlePadding_donut2: any = { left: 0, top: 20, right: 0, bottom: 10 };
    getWidth1_donut2(): any {
        if (document.body.offsetWidth < 300) {
            return "90%";
        }
        return "100%";
    }
    seriesGroups_donut2: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 48,
                    initialAngle: 300,
                    radius: 80,
                    innerRadius: 30,
                    centerOffset: 0,
                    offsetY: 80,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    formatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source_donut2.length; index++) {
                            sum += this.source_donut2[index].Share;
                        }

                        console.log(sum);
                        
                        return (this.source_donut2[itemIndex].Share/sum * 100) .toFixed(1)+ "%";
                    },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source_donut2.length; index++) {
                            sum += this.source_donut2[index].Share;
                        }

                        console.log(sum);
                        
                        return value + ": "+ (this.source_donut2[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    legendLayout_donut2: any = {
        left: 170,
        top: 180,
        width: 200,
        height: 240,
        flow: "vertical"
    };

    //Progress Bar
    horizontalColorRanges1 = [{ stop: 100, color: "#aa66cc" }];
    horizontalColorRanges2 = [{ stop: 100, color: "#ff4444" }];
    horizontalColorRanges3 = [{ stop: 100, color: "#f0ad4e" }];
    ruleValueFields: any;
    evaluationRuleValue  : any;
    selected : any;

    evaluationRuleFields = [
        {value:"field_1",valueFieldType:'sales',viewValue:"Sales"},
        {value:"field_2",valueFieldType:'nps',viewValue:"Net Promoter Score"},
        {value:"field_3",valueFieldType:'css',viewValue:"Customer Satisfaction Score"},
        {value:"field_4",valueFieldType:'pavv',viewValue:"Page and Visitor Views"},
        {value:"field_5",valueFieldType:'savcs',viewValue:"Service Availability Via customer spend"},
        {value:"field_6",valueFieldType:'clbsh',viewValue:"Customer lost/business saved hours"},
        {value:"field_7",valueFieldType:'ai',viewValue:"Active incidents"},
        {value:"field_8",valueFieldType:'as',viewValue:"Average sale"},
        {value:"field_9",valueFieldType:'cr',viewValue:"Converstion rate"},
        {value:"field_10",valueFieldType:'cvpdg',viewValue:"customer visit per day growth"},
        {value:"field_11",valueFieldType:'ts',viewValue:"Traffic source"},
        {value:"field_12",valueFieldType:'pal',viewValue:"Product affinity levels"},
        {value:"field_13",valueFieldType:'pvpv',viewValue:"Page views per visit"},
        {value:"field_14",valueFieldType:'br',viewValue:"Bounce rate"},
        {value:"field_15",valueFieldType:'lsoosi',viewValue:"Low stock/Out of stock items"},
        {value:"field_16",valueFieldType:'gsc',viewValue:"Gender segmentation change"},
        {value:"field_17",valueFieldType:'npsssc',viewValue:"NPS segment sentiment change"},
        {value:"field_18",valueFieldType:'ujtf',viewValue:"User journey traffic flow"},
        {value:"field_19",valueFieldType:'clv',viewValue:"Customer lifetime value"},
        {value:"field_20",valueFieldType:'locs',viewValue:"Loss of customer sentiment"},
        {value:"field_21",valueFieldType:'acroo5',viewValue:"Average customer review out of 5"},
        {value:"field_22",valueFieldType:'smes',viewValue:"Social Media engagement score"},
        {value:"field_23",valueFieldType:'sffl',viewValue:"Social followers & fans levels"},
        {value:"field_24",valueFieldType:'ccl',viewValue:"Concern classification level"}

    ];

    public makeChart(id) {
        var canvas: any = document.getElementById(id);
        var ctx = canvas.getContext("2d");

        var gradientStroke = ctx.createLinearGradient(0, 0, 0, 400);
        gradientStroke.addColorStop(0, "rgba(250,174,50,1)");
        gradientStroke.addColorStop(1, 'rgba(250,174,50,0)');

        var gradientFill = ctx.createLinearGradient(0, 0, 0, 400);
        gradientFill.addColorStop(0, "rgba(250,174,50,1)");
        gradientFill.addColorStop(1, "rgba(250,174,50,0)");

        var gradientStroke1 = ctx.createLinearGradient(0, 0, 0, 400);
        gradientStroke1.addColorStop(0, "rgba(183,115,222,1)");
        gradientStroke1.addColorStop(1, "rgba(183,115,222,0.2)");

        var gradientFill1 = ctx.createLinearGradient(0, 0, 0, 400);
        gradientFill1.addColorStop(0, "rgba(183,115,222,1)");
        gradientFill1.addColorStop(1, "rgba(183,115,222,0.2)");

        
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN"],
                datasets: [{
                    label:'Sales Actual',
                    labelColor:"rgba(250,174,50,1)",
                    borderColor: gradientStroke, 
                    pointBorderColor: gradientStroke,
                    pointBackgroundColor: gradientStroke,
                    pointHoverBackgroundColor: gradientStroke,
                    pointHoverBorderColor: gradientStroke,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: gradientFill,
                    borderWidth: 4,
                    data: [12, 23, 11, 10, 8, 7]
                },
                {
                    label:'Sales Forecast',
                    labelColor:"rgba(183,115,222,1)",
                    borderColor: gradientStroke1, 
                    pointBorderColor: gradientStroke1,
                    pointBackgroundColor: gradientStroke1,
                    pointHoverBackgroundColor: gradientStroke1,
                    pointHoverBorderColor: gradientStroke1,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: gradientFill1,
                    borderWidth: 4,
                    data: [12, 2, 25, 9, 6, 17]
                }]
            },
            options: {
                legend: {
                    possition:"top",
                    labels: {
                        fontColor:"blue"
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "blue",
                            fontStyle: "bold",
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            callback: function(label,index,labels){
                                return label + 'k';
                            },
                            min: 0,
                            max: 30
                        },
                        gridLines: {
                            drawTicks: false,
                            display: false
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent"
                        },
                        ticks: {
                            fontColor: "blue",
                            fontStyle: "bold"
                        }
                    }]
                }
            }
        });
        myChart.render();
    }
    ngOnInit(){
        this.makeChart("myChart1");

        var canvas: any = document.getElementById("sitevisitor_chart");
        var ctx = canvas.getContext("2d");

        var gradientStroke2 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStroke2.addColorStop(0, "rgba(204,0,23,0.97)");
        gradientStroke2.addColorStop(1, 'rgba(204,0,23,0.32)');

        var gradientFill2 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientFill2.addColorStop(0, "rgba(204,0,23,0.97)");
        gradientFill2.addColorStop(1, "rgba(204,0,23,0.32)");

        var gradientStroke3 = ctx.createLinearGradient(0, 0, 0, 400);
        gradientStroke3.addColorStop(0, "rgba(30, 136, 229, 1)");
        gradientStroke3.addColorStop(1, "rgba(30, 136, 229, 0.32)");

        var gradientFill3 = ctx.createLinearGradient(0, 0, 0, 400);
        gradientFill3.addColorStop(0, "rgba(30, 136, 229, 1)");
        gradientFill3.addColorStop(1, "rgba(30, 136, 229, 0.32)");

        
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN"],
                datasets: [{
                    label:'New Customers',
                    borderColor: gradientStroke2, 
                    pointBorderColor: gradientStroke2,
                    pointBackgroundColor: gradientStroke2,
                    pointHoverBackgroundColor: gradientStroke2,
                    pointHoverBorderColor: gradientStroke2,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: gradientFill2,
                    borderWidth: 4,
                    data: [120, 230, 110, 104, 82, 237]
                },
                {
                    label:'Returning Customers',
                    borderColor: gradientStroke3, 
                    pointBorderColor: gradientStroke3,
                    pointBackgroundColor: gradientStroke3,
                    pointHoverBackgroundColor: gradientStroke3,
                    pointHoverBorderColor: gradientStroke3,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 3,
                    fill: true,
                    backgroundColor: gradientFill3,
                    borderWidth: 4,
                    data: [120, 205, 250, 90, 486, 170]
                }]
            },
            options: {
                legend: {
                    possition:"top"

                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "rgba(30, 136, 229,1)",
                            fontStyle: "bold",
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            callback: function(label,index,labels){
                                return label + 'k';
                            },
                            min: 0,
                            max: 500
                        },
                        gridLines: {
                            drawTicks: false,
                            display: false
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent"
                        },
                        ticks: {
                            fontColor: "rgba(204,0,23,0.97)",
                            fontStyle: "bold"
                        }
                    }]
                }
            }
        });

        myChart.render();
    }

    constructor(
    )
    {
        this._registerCustomChartJSPlugin();
    }

    private _registerCustomChartJSPlugin(): void {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }

    @ViewChild('content') content:ElementRef;

    public downloadPDF(){
        let doc = new jsPDF("l", "mm", "a4");
        var width = doc.internal.pageSize.getWidth();
        var height = doc.internal.pageSize.getHeight();
        let speicalElementHandlers = {
            '#editor': function(element,renderer){
                 return true;
            }
        };

//        let content = this.content.nativeElement;
        var ids = {
            sales: "myChart1",
            nps: "canvas-nps",
            css: "canvas-css",
            pavv: "sitevisitor_chart",
            savcs: "canvas-savcs",
            clbsh: "canvas-clbsh",
            clv: "canvas-clv",
            locs: "canvas-locs",
            smes: "canvas-smes",
            sffl: "canvas-sffl"
        }
        var canvas = document.getElementById(ids [this.selected.valueFieldType]) as HTMLCanvasElement;
        var imgData = canvas.toDataURL();
        doc.addImage(imgData, 'JPEG', 0, 0,width,height/2);
        
/*
        doc.fromHTML(content, 0, 0, {
            'width': 500,
            'height': 500,
            'elementHandlers': speicalElementHandlers
        });*/

        doc.save('chart-sample.pdf');
    }
}
