import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule, GaugeModule } from '@swimlane/ngx-charts';
import { ReportingComponent } from './reporting.component';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatSelectModule, MatTabsModule, MatIcon } from '@angular/material';
import { NgCircleProgressModule } from "ng-circle-progress";
const routes = [
    {
        path     : 'reporting',
        component: ReportingComponent
    }
];

@NgModule({
    declarations: [
        ReportingComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatFormFieldModule,
        MatSelectModule,
        MatButtonModule,
        FuseSharedModule,
        ChartsModule,
        NgxChartsModule,
        MatIconModule,
        GaugeModule,
        NgCircleProgressModule.forRoot({
            radius: 100,
            outerStrokeWidth: 10,
            innerStrokeWidth: 5,
            showBackground: false,
            startFromZero: false,
            backgroundPadding: -10,
            maxPercent: 100,
            showInnerStroke: true,
            subtitle:"Uptime"
        })
    ],
    exports     : [
        ReportingComponent
    ]
})

export class ReportingModule
{
}
