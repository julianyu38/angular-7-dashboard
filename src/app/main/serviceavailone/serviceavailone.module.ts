import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { FuseSharedModule } from "@fuse/shared.module";

import { ServiceavailoneComponent } from "./serviceavailone.component";
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatSelectModule, MatTabsModule } from '@angular/material';
import { NgCircleProgressModule } from 'ng-circle-progress';
const routes = [
    {
        path: "serviceavailone",
        component: ServiceavailoneComponent
    }
];

@NgModule({
    declarations: [ServiceavailoneComponent],
    imports: [
        RouterModule.forChild(routes),
        TranslateModule,
        FuseSharedModule,
        MatFormFieldModule,
        MatIconModule,
        MatSelectModule,
        MatButtonModule,
        MatMenuModule,
        MatTabsModule,
        NgCircleProgressModule
    ],
    exports: [ServiceavailoneComponent]
})
export class ServiceavailoneModule { }
