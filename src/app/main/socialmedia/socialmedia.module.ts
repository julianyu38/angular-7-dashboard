import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { SocialmediaComponent } from './socialmedia.component';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatIconModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http'
import { BBCService, SocialmediaService } from './socialmedia.service';
import { NgxTwitterTimelineModule } from 'ngx-twitter-timeline';
const routes = [
    {
        path     : 'socialmedia',
        component: SocialmediaComponent,
        resolve : {
            data: SocialmediaService
        }
    }
];

@NgModule({
    declarations: [
        SocialmediaComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        FuseSharedModule,
        ChartsModule,
        NgxChartsModule,
        MatIconModule,
        HttpClientModule,
        NgxTwitterTimelineModule.forRoot()
    ],
    providers: [BBCService,SocialmediaService],
    exports     : [
        SocialmediaComponent
    ]
})

export class SocialmediaModule
{
}
