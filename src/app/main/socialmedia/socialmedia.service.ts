import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class BBCService {
    _bbc_url: string =
        "https://newsapi.org/v2/everything?sources=bbc-news&apiKey=ef9a2d0b56e94cae8c4b40012c04e08d";
    constructor(private bbchttp: HttpClient) {}

    getBBCNews() {
        return this.bbchttp.get(this._bbc_url);
    }
}

@Injectable()
export class SocialmediaService implements Resolve<any>{
    widgets : any[];

    constructor(private _httpClient: HttpClient){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<any> | Promise<any> | any{
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getWidgets()
            ]).then(
                () => {resolve();},
                reject
            );
        });
    }

    getWidgets() : Promise<any>{
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/socialmedia-widgets')
                .subscribe((response : any) => {
                    this.widgets = response;
                    resolve(response);
                }, reject);
        });
    }
}