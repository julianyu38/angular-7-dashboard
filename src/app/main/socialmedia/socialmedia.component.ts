import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from '@angular/router';
import { BBCService, SocialmediaService } from "./socialmedia.service";

@Component({
    selector: "socialmedia",
    templateUrl: "./socialmedia.component.html",
    styleUrls: ["./socialmedia.component.scss"],
    
})
export class SocialmediaComponent implements OnInit {
    widgets: any;
    widget1SelectedYear = "2016";
    widget5SelectedDay = "today";

    //jqxChart

    source: any = [
        { Browser: "Customer Service", Share: 4250 },
        { Browser: "Product", Share: 3970 },
        { Browser: "Price", Share: 3454 },
        { Browser: "Competition", Share: 2390 }
    ];
    dataAdapter: any = new jqx.dataAdapter(this.source, {
        async: false,
        autoBind: true,
        loadError: (xhr: any, status: any, error: any) => {
            alert('Error loading "' + this.source.url + '" : ' + error);
        }
    });
    padding1: any = { left: 5, top: 25, right: 5, bottom: 5 };
    titlePadding1: any = { left: 0, top: 20, right: 0, bottom: 10 };
    getWidth1(): any {
        if (document.body.offsetWidth < 300) {
            return "90%";
        }
        return "100%";
    }
    legendLayout: any = { left: 400, top: 85, width: 300, height: 300, flow: 'vertical' };

    seriesGroups1: any[] = [
        {
            type: "donut",
            showLabels: true,
            series: [
                {
                    dataField: "Share",
                    displayText: "Browser",
                    labelRadius: 60,
                    initialAngle: 300,
                    radius: 90,
                    innerRadius: 35,
                    centerOffset: 0,
                    offsetX: 270,
                    offsetY: 90,
                    formatSettings: { sufix: "", decimalPlaces: 0 },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source.length; index++) {
                            
                            sum += this.source[index].Share;
                        }

                        console.log(sum);
                        
                        return value + ": "+ (this.source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];

    //Progress Bar
    horizontalColorRanges1 = [{ stop: 100, color: "#aa66cc" }];
    horizontalColorRanges2 = [{ stop: 100, color: "#ff4444" }];
    horizontalColorRanges3 = [{ stop: 100, color: "#f0ad4e" }];

    //BBC News
    bbc_data = {};
    latest_news_img: string;
    latest_news_title: string;
    latest_news_content: string;
    private twitter : any;
    constructor(private bbcService: BBCService, private _router: Router, private _socialmediaservice: SocialmediaService) {
        setInterval(() => this.initTwitterWidget(), 1000);
    }

    initTwitterWidget() {
        this.twitter = this._router.events.subscribe(val => {
          if (val instanceof NavigationEnd) {
            (<any>window).twttr = (function (d, s, id) {
              let js: any, fjs = d.getElementsByTagName(s)[0],
                  t = (<any>window).twttr || {};
              if (d.getElementById(id)) return t;
              js = d.createElement(s);
              js.id = id;
              js.src = "https://platform.twitter.com/widgets.js";
              fjs.parentNode.insertBefore(js, fjs);
    
              t._e = [];
              t.ready = function (f: any) {
                  t._e.push(f);
              };
    
              return t;
            }(document, "script", "twitter-wjs"));
              (<any>window).twttr.widgets.load();
    
          }
        });

        
      }

    ngOnInit() {
        this.widgets = this._socialmediaservice.widgets;
        this.bbcService.getBBCNews().subscribe(data => {
            this.bbc_data = data;
            console.log("Articles", this.bbc_data["articles"][0]["urlToImage"]);
            this.latest_news_img = this.bbc_data["articles"][0]["urlToImage"];
            this.latest_news_title = this.bbc_data["articles"][0]["title"];
            this.latest_news_content = this.bbc_data["articles"][0]["content"];
        });
    }
}
