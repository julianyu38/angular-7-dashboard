import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { ServiceavailComponent } from './serviceavail.component';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatSelectModule, MatTabsModule } from '@angular/material';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule, GaugeModule } from '@swimlane/ngx-charts';
import { NgCircleProgressModule } from "ng-circle-progress";
import { ServiceavailService } from "./serviceavail.service";
import { BrowserModule }  from '@angular/platform-browser';
import {RoundProgressModule} from 'angular-svg-round-progressbar';

const routes = [
    {
        path     : 'serviceavail',
        component: ServiceavailComponent,
        resolve: {
            data: ServiceavailService
        }
    }
];

@NgModule({
    declarations: [
        ServiceavailComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        BrowserModule,
        FuseSharedModule,
        MatIconModule,
        MatFormFieldModule,
        MatSelectModule,
        ChartsModule,
        NgxChartsModule,
        RoundProgressModule,
        MatButtonModule,
        GaugeModule,
        MatMenuModule,
        MatTabsModule,
        NgCircleProgressModule.forRoot({
            radius: 100,
            outerStrokeWidth: 10,
            innerStrokeWidth: 5,
            showBackground: false,
            startFromZero: false,
            backgroundPadding: -10,
            maxPercent: 100,
            showInnerStroke: true,
            subtitle: "Uptime"
        })
    ],
    providers:[
        ServiceavailService
    ],
    exports     : [
        ServiceavailComponent
    ]
})

export class ServiceavailModule
{
}
