import { Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { jqxProgressBarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxprogressbar';
import { ServiceavailService } from 'app/main/serviceavail/serviceavail.service';
@Component({
    selector: 'serviceavail',
    templateUrl: './serviceavail.component.html',
    styleUrls: ['./serviceavail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ServiceavailComponent implements OnInit {
    private twitter: any;

    widgets: any;
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';
    
    // SM2M Widgets
    source : any = [
        {Browser:"Portal", Share:4250},
        {Browser:"Core", Share:3970},
        {Browser:"Opengate", Share:3454},
        {Browser:"Servers", Share:2390}   
    ];

    dataAdapter: any = new jqx.dataAdapter(this.source, { async: false, autoBind: true, loadError: (xhr: any, status: any, error: any) => { alert('Error loading "' + this.source.url + '" : ' + error); } });
    padding: any = {left: 5, top: 25, right: 5, bottom: 5};
    titlePadding: any = {left:0, top: 20, right:0, bottom: 10};
    seriesGroup : any[] = [
        {
            type : 'donut',
            showLabels: true,
            series: [
                {
                    dataField: 'Share',
                    displayText: 'Browser',
                    labelRadius: 80,
                    initialAngle: 300,
                    radius: 100,
                    innerRadius: 60,
                    centerOffset: 0,
                    offsetY:120,
                    formatSettings: { sufix: '', decimalPlaces: 0 },
                    legendFormatFunction: (value, itemIndex, serie, group) => {
                        var sum = 0;
                        for (let index = 0; index < this.source.length; index++) {
                            sum += this.source[index].Share;
                        }
                        return value + ": "+ (this.source[itemIndex].Share/sum * 100) .toFixed(2)+ "%";
                    }
                }
            ]
        }
    ];
    legendLayout: { left: 50, top: 260, width: '100%', height: 240, flow: 'horizontal'};

    //Table
    columns: any[] =
    [
        { text: '#', dataField: '#', width:'10%', align:'center', cellsAlign: 'center' },
        { text: 'Server', dataField: 'Server', width: '20%', align:'center', cellsAlign: 'center' },
        { text: 'Description', dataField: 'Description', width:'30%', align: 'center', cellsAlign: 'center' },
        { text: 'Time to Fix', dataField: 'Time to Fix', width: '20%', align: 'center', cellsAlign: 'center' },
        { text: 'Criticality', dataField: 'Criticality', width: '20%', align: 'center', cellsAlign: 'center' }
    ];

    getWidth(width) : any {
        if(document.body.offsetWidth < width){
            return '90%';
        }
        return '100%';
    }


    @ViewChild('jqxProgressBar1') jqxProgressBar1: jqxProgressBarComponent;
    @ViewChild('jqxProgressBar2') jqxProgressBar2: jqxProgressBarComponent;
    @ViewChild('jqxProgressBar3') jqxProgressBar3: jqxProgressBarComponent;

    horizontalColorRanges1 = [{ stop: 100, color: '#35b5e5' }];

    private server_semi_progressbar(server_progressbar_id,percent){
        var ProgressBar = require('progressbar.js');
        var container_mesh = document.getElementById(server_progressbar_id);
        var bar_mesh = new ProgressBar.SemiCircle(container_mesh,{
            strokeWidth: 12,
            color: '#ED6A5A',
            trailColor: '#aaa',
            trailWidth: 12,
            easing: 'easeInOut',
            duration: 1400,
            svgStyle: null,
            text: {
                value: '',
                alignToBottom: false
            },
            from: { color: '#ED6A5A' },
            to: { color: '#ED6A5A' },
            // Set default step function for all animate calls
            step: (state, bar) => {
                bar.path.setAttribute('stroke', state.color);
                var value = Math.round(bar.value() * 100);
                if (value === 0) {
                    bar.setText('');
                } else {
                    bar.setText(value + '%');
                }

                bar.text.style.color = state.color;
            }
        });

        bar_mesh.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
        bar_mesh.text.style.fontSize = '2rem';
        bar_mesh.animate(percent);
    }
    ngOnInit() {
        this.widgets = this._serviceavailservice.widgets;
        this.server_semi_progressbar("container_mesh",0.2);
        this.server_semi_progressbar("container_storage",0.3);
        this.server_semi_progressbar("container_validator",0.4);
        this.server_semi_progressbar("container_dns",0.5);
    }
    constructor(
        private _router: Router,
        private _serviceavailservice: ServiceavailService
    ) {
        this._registerCustomChartJSPlugin();
    }

    private _registerCustomChartJSPlugin(): void
    {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                )
                {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if ( !meta.hidden )
                    {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 1)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + '%';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }
}
