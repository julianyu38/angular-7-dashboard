import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { fuseConfig } from 'app/fuse-config';
import { FakeDbService } from 'app/fake-db/fake-db.service';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';

import { DashboardModule } from 'app/main/dashboard/dashboard.module';
import { ServiceavailModule} from 'app/main/serviceavail/serviceavail.module';
import { CustomersatisModule } from 'app/main/customersatis/customersatis.module';
import { EcommercemetricModule } from 'app/main/ecommercemetric/ecommercemetric.module';
import { SocialmediaModule } from 'app/main/socialmedia/socialmedia.module';
import { ReportingModule } from 'app/main/reporting/reporting.module';
import { IncidentdashboardModule } from 'app/main/incidentdashboard/incidentdashboard.module';
import { LifecycledashboardModule } from 'app/main/lifecycledashboard/lifecycledashboard.module';
import { SettingsModule } from 'app/main/settings/settings.module';
import { ServiceavailoneModule } from 'app/main/serviceavailone/serviceavailone.module';
import { ServiceavailtwoModule } from 'app/main/serviceavailtwo/serviceavailtwo.module';
import { EcommercemetriconeModule } from 'app/main/ecommercemetricone/ecommercemetricone.module';

const appRoutes: Routes = [
    {
        path      : '**',
        redirectTo: 'dashboard'
    }
];

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports     : [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        // jqxChartComponent,
        LayoutModule,
        DashboardModule,
        ServiceavailModule,
        CustomersatisModule,
        EcommercemetricModule,
        SocialmediaModule,
        ReportingModule,
        IncidentdashboardModule,
        LifecycledashboardModule,
        SettingsModule,
        ServiceavailoneModule,
        ServiceavailtwoModule,
        EcommercemetriconeModule
    ],
    bootstrap   : [
        AppComponent
    ],
    exports: [
        CommonModule
    ]
})
export class AppModule
{
}
