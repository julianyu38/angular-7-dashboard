export const locale = {
    lang: 'tr',
    data: {
        'NAV': {
            'APPLICATIONS': 'Programlar',
            'DASHBOARD'        : {
                'TITLE': 'Örnek',
                'BADGE': '15'
            },
            'CUSTOMERSATIS'    :{
                'TITLE': 'Customer Satisfication'
            }
        }
    }
};
