export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'DASHBOARD'        : {
                'TITLE': 'Dashboard',
                'BADGE': '25'
            },
            'SERVICEAVAIL'    :{
                'TITLE': 'Service Availability'
            },
            'CUSTOMERSATIS'    :{
                'TITLE': 'Customer Satisfication'
            },
            'ECOMMERCEMETRIC'    :{
                'TITLE': 'Ecommerce Metrics'
            },
            'SOCIALMEDIA'    :{
                'TITLE': 'Social Media'
            },
            'REPORTING'    :{
                'TITLE': 'Reporting'
            },
            'INCIDENTDASHBOARD'    :{
                'TITLE': 'Incident Dashboard'
            },
            'LIFECYCLEDASHBOARD'    :{
                'TITLE': 'LifeCycle Dashboard'
            },
            'SETTINGS'    :{
                'TITLE': 'Settings'
            },
            'SERVICEAVAILONE'    :{
                'TITLE': 'ServiceAvailability1'
            },
            'SERVICEAVAILTWO'    :{
                'TITLE': 'ServiceAvailability2'
            },
            'ECOMMERCEMETRICONE' :{
                'TITLE': 'Ecommerce Metric1'
            }


        }
    }
};
