import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        children: [
            {
                id: 'dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/dashboard'
            },
            {
                id: 'serviceavail',
                title: 'Service Availability',
                translate: 'NAV.SERVICEAVAIL.TITLE',
                type: 'collapsable',
                icon: 'bubble_chart',
                url: '/serviceavail',
                children: [
                    {
                        id: 'serviceavailone',
                        title: 'ServiceAvailability1',
                        translate: 'NAV.SERVICEAVAILONE.TITLE',
                        type: 'item',
                        icon: 'bubble_chart',
                        url: '/serviceavailone'
                    },
                    {
                        id: 'serviceavailtwo',
                        title: 'ServiceAvailability2',
                        translate: 'NAV.SERVICEAVAILTWO.TITLE',
                        type: 'item',
                        icon: 'bubble_chart',
                        url: '/serviceavailtwo'
                    }

                ]
            },
            {
                id: 'customersatis',
                title: 'Customer Satisfication',
                translate: 'NAV.CUSTOMERSATIS.TITLE',
                type: 'item',
                icon: 'tag_faces',
                url: '/customersatis'
            },
            {
                id: 'ecommercemetric',
                title: 'Ecommerce Metrics',
                translate: 'NAV.ECOMMERCEMETRIC.TITLE',
                type: 'collapsable',
                icon: 'shopping_cart',
                url: '/ecommercemetric',
                children: [
                    {
                        id: 'ecommercemetricone',
                        title: 'EcommerceMetric1',
                        translate: 'NAV.ECOMMERCEMETRICONE.TITLE',
                        type: 'item',
                        icon: 'shopping_cart',
                        url: '/ecommercemetricone'
                    }
                ]
            },
            {
                id: 'socialmedia',
                title: 'Social Media',
                translate: 'NAV.SOCIALMEDIA.TITLE',
                type: 'item',
                icon: 'cloud_circle',
                url: '/socialmedia'
            },
            {
                id: 'reporting',
                title: 'Reporting',
                translate: 'NAV.REPORTING.TITLE',
                type: 'item',
                icon: 'report',
                url: '/reporting'
            },
            {
                id: 'incidentdashboard',
                title: 'Incident Dashboard',
                translate: 'NAV.INCIDENTDASHBOARD.TITLE',
                type: 'item',
                icon: 'spa',
                url: '/incidentdashboard'
            },
            {
                id: 'lifecycledashboard',
                title: 'LifeCycle Dashboard',
                translate: 'NAV.LIFECYCLEDASHBOARD.TITLE',
                type: 'item',
                icon: 'public',
                url: '/lifecycledashboard'
            },
            {
                id: 'settings',
                title: 'Settings',
                translate: 'NAV.SETTINGS.TITLE',
                type: 'item',
                icon: 'settings',
                url: '/settings'
            }
        ]
    }
];
